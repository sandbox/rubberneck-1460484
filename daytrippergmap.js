/**
 * @preserve
 *    dayTripper gmap Plan It For Me Google Map API
 *    Configure the Google Map API V3
 *    http://rubberneckdesigns/
 *
 */

var directionsService = new google.maps.DirectionsService();
var geocoder;
var map;
var directionsDisplay;
var directionsService;
var stepDisplay;
var markerArray = [];
var stepInstructions = '';

(function ($) {
	
  Drupal.behaviors.dayTrippergmap = {
    attach: function (context, settings) {
      // START ///////////////  Day Tripper jQuery Behaviors ///////////////////
      //$('').

      initialize();
      
		  // Button: "Recalculate" button has been clicked
		 $('#but-add-recalc').click(function(){
          // This function is not done yet
		         getTimesOfCheckedEvents();
		  });
		  
		  // END ///////////////  Day Tripper jQuery Behaviors ///////////////////
    }// attach: function (context, settings)
  };// Drupal.behaviors.daytrippergmap


/*
returns an array of eventID, Time paired results
*/
function getTimesOfCheckedEvents(){
//Configure vars
var agenda = [];
//var addr = '';
//var points = [];
//var waypoints = [];
var minPerHr = 60; // dugh!
var qtyHrsInAgenda = $('#daytripper_how_many_hours').val();
//Get the admin provided variables (hidden inputs)
//var minutesPerPixel =  2;daytripper_minutes_per_pixels
//var parseFloat( startHour ) = 7;
//Get the admin provided variables (hidden inputs)
var minutesPerPixel = $('#daytripper_minutes_per_pixels').val();
var startHour = $('#daytripper_start_hour').val();
var pxPerHour = ( minPerHr / parseFloat( minutesPerPixel ) );
//Calculate the offset (compensates for the agenda's start hour not at 0 )
var basePos = ( parseFloat( startHour ) * pxPerHour );
// do not use .each()'s i below as counter bc !is(':checked') will create 'undefined' in object
var agenda_cnt = 0;
// Iterate over the events that have been checked
$('.view-id-day_tripper_events li div.daytripper-event').each(function(i,e){
    if( $(this).find('.daytripper-checkbox :checkbox').is(':checked') ){
				// Get the x and y coordinates
				var X_val = $(this).position().left;
				var Y_val = $(this).position().top;
				var event_ID = $(this).attr('id');
				//Calculate the scheduled time
				var skedTime  = ( ( Y_val / pxPerHour ) + parseFloat( startHour ) );
        // Store the schedule info in an array 
        agenda[agenda_cnt] = { 
                                              eventID: event_ID, 
                                              schedTime: skedTime,
                                              address: $(this).find('.daytripper-addr-info').text()
                                            };
        agenda_cnt++;
		}//if  										
});

if( agenda.length > 0 ){
	// Sort the agend by time
	agenda.sort( function(a, b){ return a.schedTime - b.schedTime })	 ;
	// Add my-start-address to front of array
	agenda.unshift( { 
	                           eventID: 'my-start-address', 
	                           schedTime: 0,
                             address: document.getElementById("my-start-address").value   
                           });
	//get the first event
	var startPoint = agenda[0].address;
	//get the last event
	var endPoint = agenda[agenda.length-1].address;
	// Get the waypoints (every point address in between start and end)
	var wayPoints = [];
	var j = 0;
	for( j = 0; j < agenda.length; j++ ){
		if( agenda[j] != agenda[0] && agenda[j].address != endPoint ){
			 // Build the wayPaoints array
				wayPoints.push( agenda[j].address ) ;
		}// if
	}// for	
	calcRoute( startPoint , endPoint, wayPoints ) ;
}//if( agenda.length > 0 )
else{
 alert( "Events need to be checked to be included in the recalculation.");
}
	 // AJAX validate the code		
	// var loadUrl = '/daytripper/ajax/update-agenda/'+ sched;  
	// $('#daytripper-agenda-data').load( loadUrl );  	
}//function
    
})(jQuery);

function initialize() {
  // Instantiate a directions service.
  directionsService = new google.maps.DirectionsService();
  
  // Create a map and center it on Cleveland.
  var cleveland = new google.maps.LatLng(41.7, -81.68);
  
  var myOptions = {
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: cleveland
  }
  
  map = new google.maps.Map( document.getElementById("map_canvas"), myOptions );
  
// Create a renderer for directions and bind it to the map.
  var rendererOptions = {
    map: map
  }
  
  directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);

  // Instantiate an info window to hold step text.
  //stepDisplay = new google.maps.InfoWindow();
 //
}

     
function codeAddress( address ) {
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
  
function calcRoute( start, end, wayPoints ) {
////// ~~~
  // First, clear out any existing markerArray
  // from previous calculations.
  for (i = 0; i < markerArray.length; i++) {
    markerArray[i].setMap(null);
  }
////// ~~~
////// ----   
  var waypts = [];
  for (var i = 0; i < wayPoints.length; i++) {
      waypts.push({
          location:wayPoints[i],
          stopover:true
        });
  }//for
////// ----   
////// ---- 
  var request = {
      origin: start,
      destination: end,
      waypoints: waypts,
      optimizeWaypoints: false,
      travelMode: google.maps.TravelMode.DRIVING
  };
////// ----   


  directionsService.route(request, function(response, status) {
console.log( "RESPONS: " + response ) ;
    if (status == google.maps.DirectionsStatus.OK) {
     var warnings = document.getElementById("warnings_panel");
      warnings.innerHTML = "" + response.routes[0].warnings + "";
      directionsDisplay.setDirections(response);
      showSteps(response);
      }//if    
  });
  
}


function showSteps(directionResult) {
  // For each step, place a marker, and add the text to the marker's
  // info window. Also attach the marker to an array so we
  // can keep track of it and remove it when calculating new
  // routes.
 //  var myRoute = directionResult.routes[0].legs[0];
    var myRoutes = directionResult.routes;
console.log( "My Routs", myRoutes );
var instructText = "INSTRUCTIONS:" ;   
   for (var i = 0; i < myRoutes.length; i++) {
console.log( "My Routs. legs", myRoutes[i].legs );
      var myLegs = myRoutes[i].legs ;
      for (var ii = 0; ii < myLegs.length; ii++) {
      instructText  += '<span class="gmap-directions-label">To: '+ myLegs[ii].end_address + '</span>';   
         var mySteps = myLegs[ii].steps
         for (var iii = 0; iii < mySteps.length; iii++) {
              instructText  += mySteps[iii].instructions;     
         }//for
      }//for         
   }//for

document.getElementById('directions_panel').innerHTML = instructText ;
}



function attachInstructionText( marker, text ) {
  console.log( "MARKER:", marker );
  google.maps.event.addListener( marker, 'click', function() {
     alert( "Marker Clicked: "  );
         
  console.log( "Passed marker:", marker );     
// var existingText = document.getElementById('directions_panel').innerHTML;
// var newText = existingText + text;
// alert( "New Text: " +  newText   );
// document.getElementById('directions_panel').innerHTML = newText   ; 
  });

}